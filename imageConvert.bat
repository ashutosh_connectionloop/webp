@echo off
set $o="c:\Projects\ImageConverter\target\"
for %%i in (c:\Projects\ImageConverter\source\*.*) do (
  echo %%i
  cwebp -q 80 %%i -o c:\Projects\ImageConverter\target\%%~ni.webp
)
